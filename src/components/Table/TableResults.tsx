import React, { ChangeEvent } from "react";
import { useDispatch } from "react-redux";

import {
    TableBody,
    TableCell,
    TableFooter,
    TableHead,
    Table,
    TableRow,
    Paper,
    TableContainer,
} from "@material-ui/core";
import { Pagination } from "@material-ui/lab";
import classes from "./TableResults.module.scss";

import {
    searchActions,
    WikiSearchElementType,
} from "../../store/reducers/search-reducer";

type TableResultsPropsType = {
    results: Array<WikiSearchElementType>;
    currentPage: number;
};

const TableResults: React.FC<TableResultsPropsType> = ({
    results,
    currentPage,
}) => {
    const dispatch = useDispatch();

    const currentPageResults = results.filter((res, i) => {
        if (currentPage === 1) {
            return i + 1 >= currentPage && i + 1 <= Number(`${currentPage}0`);
        } else {
            return (
                i >= Number(`${currentPage - 1}0`) &&
                i + 1 <= Number(`${currentPage}0`)
            );
        }
    });
    const totalPages = Math.ceil(results.length / 10);

    const pageChange = (
        e: ChangeEvent<unknown>,
        page: number = currentPage
    ) => {
        dispatch(searchActions.changeCurrentPage(page));
    };

    return (
        <TableContainer component={Paper}>
            <Table className={classes.table} aria-label="simple table">
                <TableHead>
                    <TableRow>
                        <TableCell>Title</TableCell>
                        <TableCell align="left">Page id</TableCell>
                        <TableCell align="left">Snippet</TableCell>
                        <TableCell align="left">Time</TableCell>
                    </TableRow>
                </TableHead>
                <TableBody>
                    {currentPageResults.map((res) => (
                        <TableRow key={res.pageid}>
                            <TableCell
                                style={{ maxWidth: 200 }}
                                component="th"
                                scope="row"
                            >
                                {res.title}
                            </TableCell>
                            <TableCell align="left">{res.pageid}</TableCell>
                            <TableCell align="left">{res.snippet}</TableCell>
                            <TableCell align="left">{res.timestamp}</TableCell>
                        </TableRow>
                    ))}
                </TableBody>
                <TableFooter className={classes.footerRow}>
                    <TableRow>
                        <td>
                            <Pagination
                                className={classes.pagination}
                                style={{ flexWrap: "nowrap" }}
                                count={totalPages}
                                shape="rounded"
                                onChange={pageChange}
                                page={currentPage}
                            />
                        </td>
                    </TableRow>
                </TableFooter>
            </Table>
        </TableContainer>
    );
};

export default TableResults;
