import React, { MouseEvent, useState } from "react";
import { useDispatch, useSelector } from "react-redux";

import { Button, TextField } from "@material-ui/core";
import styles from "./Search.module.scss";

import { fetchResultsTC } from "../../store/reducers/search-reducer";
import { AppRootStateType } from "../../store/store";

const Search: React.FC = () => {
    const dispatch = useDispatch();
    const rememberSearchText = useSelector<AppRootStateType, string>(
        (state) => state.search.rememberSearchText
    );
    const [searchText, setSearchText] = useState<string>("");

    const onSearch = (e: MouseEvent<HTMLButtonElement>) => {
        e.preventDefault();
        if (searchText.trim() !== "" && rememberSearchText !== searchText) {
            dispatch(fetchResultsTC(searchText));
        }
    };

    return (
        <form className={styles.form}>
            <TextField
                id="standard-basic"
                label="Search"
                value={searchText}
                onChange={(e) => setSearchText(e.currentTarget.value)}
            />
            <Button type="submit" variant="outlined" onClick={onSearch}>
                Search
            </Button>
        </form>
    );
};

export default Search;
